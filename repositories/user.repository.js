import fs from 'fs';

const userlist = './data/userlist.json';

const saveData = data => {
    fs.writeFile(userlist, data, function (err) {
        if (err) {
            return console.log(err);
        }
        console.log("The file was saved!");
    });
};

const getUsers = next => {
    fs.readFile(userlist, (err, data) => {
        if (err) return next(err);
        const users = JSON.parse(data);
        next(null, users);
    });
};

const getUserById = (params, next) => {
    fs.readFile(userlist, (err, data) => {
        if (err) return next(err);
        const users = JSON.parse(data);
        const user = users.filter(item => item._id === params.id);
        next(null, user);
    });
};

const deleteUserById = (params, next) => {
    fs.readFile(userlist, (err, data) => {
        if (err) return next(err);
        const users = JSON.parse(data);
        const removed = users.filter(item => item._id !== params.id);
        if (users.length !== removed.length)
            saveData(JSON.stringify(removed));
        next(null, removed);
    });
};

const updateUserById = (user, next) => {
    fs.readFile(userlist, (err, data) => {
        if (err) return next(err);
        let users = JSON.parse(data);
        for (let i = 0; i < users.length; i++) {
            if (users[i]._id == user.id) {
                users[i].name = user.name;
                users[i].health = user.health;
                users[i].attack = user.attack;
                users[i].defense = user.defense;
                users[i].src = user.source;
                break;
            }
        }
        saveData(JSON.stringify(users));
        next(null, users);
    });
};

const addUser = (user, next) => {
    fs.readFile(userlist, (err, data) => {
        if (err) return next(err);
        let users = JSON.parse(data);
        let sort = users.sort((a, b) => b._id - a._id);
        const nextId = sort ? (+sort[0]._id) + 1 : 1;
        users.push({
            _id: `${nextId}`,
            name: user.name,
            health: user.health,
            attack: user.attack,
            defense: user.defense,
            src: user.source
        });
        console.log('='+user.source+'=');
        saveData(JSON.stringify(users));
        next(null, users);
    });
};

export {
    getUsers,
    getUserById,
    deleteUserById,
    updateUserById,
    addUser
};