import express from 'express';
import {
    getUsers,
    getUserById,
    deleteUserById,
    updateUserById,
    addUser
} from '../repositories/user.repository';

const router = express.Router();

router.use((req, res, next) => {
    next();
});

/**
 * GET: /user
 */
router.get('/', (req, res) => {
    getUsers((err, users) => {
        if (err)
            res.status(400).send({
                message: `Error: ${err}`
            });
        res.send(users);
    });
});

/**
 * GET: /user/:id
 */
router.get('/:id', (req, res) => {
    const params = {
        id: req.params.id
    };

    getUserById(params, (err, user) => {
        if (err)
            res.status(400).send({
                message: `Error: ${err}`
            });
        if (!user)
            res.send({
                message: `User not found.`
            });
        else
            res.send(user);
    });
});

/**
 * DELETE: /user/:id
 */
router.delete('/:id', (req, res) => {
    const params = {
        id: req.params.id
    };
    
    deleteUserById(params, (err, users) => {
        if (err)
            res.status(400).send({
                message: `Error: ${err}`
            });
        res.send(users);
    });
});

/**
 * PUT: /user:id
 */
router.put('/:id', (req, res) => {
    const data = req.body;
    
    updateUserById(data, (err, users) => {
        if (err)
            res.status(400).send({
                message: `Error: ${err}`
            });
        res.send(users);
    });
});

/**
 * POST: /user
 */
router.post('/', (req, res) => {
    const data = req.body;

    addUser(data, (err, users) => {
        if (err)
            res.status(400).send({
                message: `Error: ${err}`
            });
        res.send(users);
    });
});

export default router;