import express from 'express';
import bodyParser from 'body-parser';
import indexRouter from './routes/index';
import usersRouter from './routes/users';

const app = express();
const port = process.env.PORT || '3000';

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use('/', express.static('public'));
app.use('/', indexRouter);
app.use('/user', usersRouter);

app.listen(port, () => console.log('app listening on port: ' + port));

module.exports = app;