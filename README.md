# stage-2-express-yourself-with-nodejs

This project implements a web server using **Node.js + Express.js** that can handle the list of following requests:

- **GET: /user** (get an array of all users)

- **GET: /user/:id** (get one user by ID)

- **POST: /user** (create user according to the data from the request body)

- **PUT: /user/:id** (update user according to the data from the request body)

- **DELETE: /user/:id** (delete one user by ID)

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

First of all, you will need [Node.js](https://nodejs.org) of version `10.3.0` or compatible with it, [npm](https://www.npmjs.com/) version `6.9.0` or compatible, and [git](https://git-scm.com/downloads) `2.17.1` or compatible.

Check if everything is OK by running `npm -v`, `node -v` and `git --version` in the CLI, it should look simmilar to this:

```bash
> npm -v
6.9.0
```

```bash
> node -v
v10.3.0
```

```bash
> git --version
git version 2.17.1.windows.2
```

### Installing

1. `git clone https://annabell_flemm@bitbucket.org/annabell_flemm/stage-2-express-yourself-with-nodejs.git`
2. `cd stage-2-express-yourself-with-nodejs`
3. `npm i` or `npm install`

### Development and build

After installing the packages, you sould be able to run these commands:

- `npm run dev` to launch a development server with [nodemon](https://www.npmjs.com/package/nodemon)
- `npm run start` to just launch a server

## Deployment

Deploying with [Heroku](https://devcenter.heroku.com/) using [Git](https://devcenter.heroku.com/articles/git).
Check the result by clicking on the [link](https://express-yourself-with-nodejs.herokuapp.com/).

***

But [Free Dyno Hours](https://devcenter.heroku.com/articles/free-dyno-hours) can lead to slower page loading.

