'use strict';

/**
 * set basic validation for user health, attack, defense
 * they must be only Numbers
 */
function validate(data) {
    return isFinite(data.health) &&
        isFinite(data.attack) && isFinite(data.defense);
}

/**
 * Generate user list with data from list.json
 */
function setItemView(user) {
    return `
        <tr data-userId="${user._id}">
            <td class="user-name" contenteditable="true">${user.name}</td>
            <td class="user-health" contenteditable="true">${user.health}</td>
            <td class="user-attack" contenteditable="true">${user.attack}</td>
            <td class="user-defense" contenteditable="true">${user.defense}</td>
            <td class="user-img" contenteditable="${user._id == -1 ? 'true' : 'false'}">
               <img class="fit-picture user-source" src="${user.source}" alt="${user.name}" />
               <button class="button button-src ${user._id == -1 ? 'hidden' : ''}" onclick="getSrcById(${user._id})">...</button>
            </td>
            <td>
                <button class="button button-get-user ${user._id == -1 ? 'hidden' : ''}" onclick="getUserById(${user._id})">*</button>
                <button class="button button-close ${user._id == -1 ? 'hidden' : ''}" onclick="deleteUserById(${user._id})">&times;</button>
                <button class="button button-update ${user._id == -1 ? 'hidden' : ''}" onclick="updateUserById(${user._id})">✎</button>
                <button class="button button-add ${user._id != -1 ? 'hidden' : ''}" onclick="addUser()">+</button>
            </td>
        </tr>`;
}

function getSrcById(id) {
    const img = document.querySelector(`tr[data-userId='${id}'] img`);
    const td = document.querySelector(`tr[data-userId='${id}'] .user-img`);
    td.textContent = img.src;
    td.setAttribute('contenteditable', true);
    img.classList.add('hidden');
}
/**
 * set first row in the table to empty for add fields
 */
function setUserView(view) {
    const emptyUser = {
        _id: -1,
        name: '',
        health: '',
        attack: '',
        defense: '',
        source: ''
    };

    return setItemView(emptyUser) + view;
}

/** 
 * update information in table after crud for update
*/
function updateUserById(id) {
    const userItem = document.querySelector(`tr[data-userid="${id}"]`);
    const data = {
        id,
        name: userItem.querySelector('.user-name').innerText,
        health: userItem.querySelector('.user-health').innerText,
        attack: userItem.querySelector('.user-attack').innerText,
        defense: userItem.querySelector('.user-defense').innerText,
        source: userItem.querySelector('.user-img').innerText || userItem.querySelector('.user-source').src
    }

    if (validate(data)) {
        fetch(`/user/${id}`, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(data => data.json())
            .then(data => {
                let userView = "";
                const table = document.querySelector('#users tbody');
                data.forEach(item => {
                    userView += setItemView(item);
                });
                table.innerHTML = setUserView(userView);
            })
            .catch(err => console.log(err));
    } else {
        alert('Incorrect data!');
    }
}

/** 
 * update information in table after crud for add new
*/
function addUser() {
    const userItem = document.querySelector(`tr[data-userid="-1"]`);
    const data = {
        id: -1,
        name: userItem.querySelector('.user-name').innerText,
        health: userItem.querySelector('.user-health').innerText,
        attack: userItem.querySelector('.user-attack').innerText,
        defense: userItem.querySelector('.user-defense').innerText,
        source: userItem.querySelector('.user-img').innerText || userItem.querySelector('.user-source').src
    }

    if (validate(data)) {
        fetch(`/user`, {
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        })
            .then(data => data.json())
            .then(data => {
                let userView = "";
                const table = document.querySelector('#users tbody');
                data.forEach(item => {
                    userView += setItemView(item);
                });
                table.innerHTML = setUserView(userView);
            })
            .catch(err => console.log(err));
    } else {
        alert('Incorrect data!');
    };
}

/** 
 * update information in table after crud for delete
*/
function deleteUserById(id) {
    fetch(`/user/${id}`, {
        method: 'DELETE'
    })
        .then(data => data.json())
        .then(data => {
            let userView = "";
            const table = document.querySelector('#users tbody');
            data.forEach(item => {
                userView += setItemView(item);
            });
            table.innerHTML = setUserView(userView);
        })
        .catch(err => console.log(err));
}

/** 
 * update information in table after crud for get user by id
*/
function getUserById(id) {
    fetch(`/user/${id}`, {
        method: 'GET'
    })
        .then(data => data.json())
        .then(data => {
            let userView = "";
            const table = document.querySelector('#users tbody');
            data.forEach(item => {
                userView += setItemView(item);
            });
            table.innerHTML = setUserView(userView);
        })
        .catch(err => console.log(err));
}

/** 
 * show all users on the screen
*/
function getUsers() {
    fetch('/user', {
        method: 'GET'
    })
        .then(data => data.json())
        .then(data => {
            let userView = "";
            const table = document.querySelector('#users tbody');
            data.forEach(item => {
                userView += setItemView(item);
            });
            table.innerHTML = setUserView(userView)
        })
        .catch(err => console.log(err));
}

function ready() {
    getUsers();
}

document.addEventListener('DOMContentLoaded', ready);